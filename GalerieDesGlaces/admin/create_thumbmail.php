<?php
/**
 * Created by PhpStorm.
 * User: dahevos
 * Date: 11/07/15
 * Time: 16:06
 */

include_once('../tools/constants.php');
include_once('../tools/tools.php');

if ($ADMIN) {
    set_error_handler(function() { /* ignore errors */ });
    dns_get_record();

    $file = explode('/', $_GET['file']);
    $folderName = $file[0];
    $photo = $file[1];

// load image and get image size
    $img = imagecreatefromfile($DIR . '/' . $folderName . '/' . $photo);

    $width = imagesx($img);
    $height = imagesy($img);

// calculate thumbnail size
    $new_width = $THUMBMAIL_WIDTH;
    $new_height = floor($height * ($THUMBMAIL_WIDTH / $width));

// create a new temporary image
    $tmp_img = imagecreatetruecolor($new_width, $new_height);

// copy and resize old image into new image
    imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

    if (!file_exists("{$THUMBMAIL_DIR}/{$folderName}")) {
        mkdir("{$THUMBMAIL_DIR}/{$folderName}", 0777, true);
    }

// save thumbnail into a file
    imagesavefromfile($DIR . '/' . $folderName . '/' . $photo, $tmp_img, "{$THUMBMAIL_DIR}/{$folderName}/{$photo}");


// calculate thumbnail size
    $new_width = $BIG_THUMBMAIL_WIDTH;
    $new_height = floor($height * ($BIG_THUMBMAIL_WIDTH / $width));

// create a new temporary image
    $tmp_img = imagecreatetruecolor($new_width, $new_height);

// copy and resize old image into new image
    imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

    if (!file_exists("{$BIG_THUMBMAIL_DIR}/{$folderName}")) {
        mkdir("{$BIG_THUMBMAIL_DIR}/{$folderName}", 0777, true);
    }

// save thumbnail into a file
    imagesavefromfile($DIR . '/' . $folderName . '/' . $photo, $tmp_img, "{$BIG_THUMBMAIL_DIR}/{$folderName}/{$photo}");

    print_r("{$folderName}/{$photo}");
    restore_error_handler();
}