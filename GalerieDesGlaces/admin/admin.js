var app = angular.module('app', []);

app.controller('AdminController', function(AdminService) {

    /* PUBLIC VAR */
    var vm = this;
    vm.logs = [];

    vm.start = function() {
        // we will first remove the thumbmail
        vm.logs.push("{0} - Start cleaning thumbmail".format(getLogTime()));
        AdminService.cleanThumbmail().then(
            function callback(data) {
                if (data) {
                    vm.logs.push("{0} - Success cleaning thumbmail".format(getLogTime()));
                    startSecondStep();
                }
                else {
                    vm.logs.push("{0} - Failed cleaning thumbmail. Please check your permissions".format(getLogTime()));
                }
            });
    };

    function startSecondStep() {
        vm.logs.push("{0} - Generating thumbmail in progress... please wait ".format(getLogTime()));
        AdminService.getPictureToProcess().then(
            function callback(data) {
                if (data) {
                    vm.logs.push("{0} - {1} files to process".format(getLogTime(), data.length));
                    startThirdStep(data);
                }
                else {
                    vm.logs.push("{0} - Failed generating thumbmail. Please check your permissions".format(getLogTime()));
                }
            });
    }

    function startThirdStep(pictures) {
        for(var key in pictures) {
            var picture = pictures[key];
            AdminService.generateThumbmail(picture).then(
                function callback(data) {
                    if (data) {
                        vm.logs.push("{0} - {1} processed".format(getLogTime(), data));
                    }
                    else {
                        vm.logs.push("{0} - Failed generating thumbmail for the picture {1}. Please check your permissions".format(getLogTime(), data));
                    }
                });
        }
    }

});

app.service("AdminService", function($http) {

    // Return public API.
    return({
        cleanThumbmail: cleanThumbmail,
        generateThumbmail: generateThumbmail,
        getPictureToProcess : getPictureToProcess
    });

    function cleanThumbmail() {
        return $http.get('./cleaning_thumbmail.php').then(
            function success(response) {
                return response.data === "true";
            },
            function error(error) {
                console.error("{0} {1} {2} {3}".format(error.config.method,error.config.url,error.status,error.statusText));
                return null;
            });
    }

    function getPictureToProcess() {
        return $http.get('../api/photos.php?folder=GalerieDesGlaces_ALLFOLDER').then(
            function success(response) {
                return response.data;
            },
            function error(error) {
                console.error("{0} {1} {2} {3}".format(error.config.method,error.config.url,error.status,error.statusText));
                return null;
            }
        );
    }

    function generateThumbmail(picture) {
        return $http.get('./create_thumbmail.php?file=' + picture).then(
            function success(response) {
                return response.data;
            },
            function error(error) {
                console.error("{0} {1} {2} {3}".format(error.config.method, error.config.url, error.status, error.statusText));
                return null;
            }
        );
    }
});



// Useful method to format string
String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

// Useful method to get current time into string
function getLogTime() {
    var date = new Date;
    var seconds = date.getSeconds();
    var minutes = date.getMinutes();
    var hour = date.getHours();
    return hour + ':' + minutes + ':' + seconds + ' ';
}
