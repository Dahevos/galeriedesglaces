<?php
/**
 * Created by PhpStorm.
 * User: dahevos
 * Date: 11/07/15
 * Time: 16:33
 */


include_once('../tools/constants.php');
include_once('../tools/tools.php');

if ($ADMIN) {

    set_error_handler(function() { /* ignore errors */ });
    dns_get_record();
    $result1 = deleteDir($THUMBMAIL_DIR);
    $result2 = deleteDir($BIG_THUMBMAIL_DIR);
    $result3 = mkdir($THUMBMAIL_DIR, 0777, true);
    $result4 = mkdir($BIG_THUMBMAIL_DIR, 0777, true);

    $result = $result1 && $result2 && $result3 && $result4;
    print_r(json_encode($result));
    restore_error_handler();
}