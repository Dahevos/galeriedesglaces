var app = angular.module('app', []);

app.controller('AppController', function(AppService, $timeout) {

    /* PUBLIC VAR */
    var vm = this;
    vm.translations = translations;
    vm.folderList = [];
    vm.selectedFolder = null;
    vm.pictureList = [];
    vm.selectedPicture = null;
    vm.selectedPictureIndex = null;

    /* PUBLIC METHODS */
    vm.selectFolder = function() {
        // we remove focus on select to avoid changing categories instead changing picture
        document.getElementById('selectFolder').blur();
        AppService.getPictures(vm.selectedFolder).then(
            function(data) {
                if (data) {
                    vm.pictureList = data;
                    vm.selectedPictureIndex = 0;
                    vm.selectedPicture = vm.pictureList[0];
                    $timeout(function() {
                        document.getElementById(vm.selectedPicture).scrollIntoView();
                    }, 100);
                }
            }
        );
    };

    vm.selectNextPicture = function() {
        if (vm.selectedPictureIndex < vm.pictureList.length - 1) {
            vm.selectedPictureIndex++;
            $timeout(function() { vm.selectedPicture = vm.pictureList[vm.selectedPictureIndex];
                document.getElementById(vm.selectedPicture).scrollIntoView();
            }, 100);
        }
    };

    vm.selectPreviousPicture = function() {
        if (vm.selectedPictureIndex > 0) {
            vm.selectedPictureIndex--;
            $timeout(function() { vm.selectedPicture = vm.pictureList[vm.selectedPictureIndex];
                document.getElementById(vm.selectedPicture).scrollIntoView();
            }, 100);
        }
    };

    vm.selectPictureIndex = function(index) {
        if (index >= 0 && index < vm.pictureList.length) {
            vm.selectedPictureIndex = index;
            $timeout(function() { vm.selectedPicture = vm.pictureList[vm.selectedPictureIndex];}, 100);
        }
    };

    vm.downloadSelectedPicture = function() {
        document.getElementById('downloadButton').click();
    };

    vm.manageKeyUp = function(event) {
        switch(event.keyCode) {
            case 39 : // right arrow
            case 40 : // up arrow
                vm.selectNextPicture();
                break;
            case 37: // left arrow
            case 38: // down arrow
                vm.selectPreviousPicture();
                break;
            case 13: // enter key
                vm.downloadSelectedPicture();
                break;
        }
    };

    vm.closeSidePanel = function() {
        document.getElementById('sidepanel').className = "sidePanel";
        document.getElementById('sidepanel').className += " sidepanelToClose";
    };

    vm.openSidePanel = function() {
        document.getElementById('sidepanel').className = "sidePanel";
        document.getElementById('sidepanel').className += " sidepanelToOpen";
    };

    vm.openInfoModal = function() {
        document.getElementById('modalInformation').className = "modalInformation";
        document.getElementById('modalInformation').className += " infoModalToOpen";
    };

    vm.closeInfoModal = function() {
        document.getElementById('modalInformation').className = "modalInformation";
        document.getElementById('modalInformation').className += " infoModalToClose";
    };

    /* INIT */
    AppService.getFolders().then(
        function callback(data) {
            if (data) {
                vm.folderList = data;
                vm.folderList.unshift(vm.translations.ALL_FOLDER);
                vm.selectedFolder = vm.folderList[0];
                vm.selectFolder();
            }
            else {
                alert(vm.translations.GET_FOLDER_ERROR);
            }
        }
    );

});

app.service("AppService", function($http) {

    // Return public API.
    return({
        getFolders: getFolders,
        getPictures : getPictures
    });

    function getFolders() {
        return $http.get('./api/folders.php').then(
            function success(response) {
                return response.data;
            },
            function error(error) {
                console.error("{0} {1} {2} {3}".format(error.config.method,error.config.url,error.status,error.statusText));
                return null;
            }
        );
    }

    function getPictures(folderName) {
        // If we ask the folder "ALL", we change the translations value with the server constant
        if (folderName == translations.ALL_FOLDER) {
            folderName = "GalerieDesGlaces_ALLFOLDER";
        }

        return $http.get('./api/photos.php?folder=' + folderName).then(
            function success(response) {
                return response.data;
            },
            function error(error) {
                console.error("{0} {1} {2} {3}".format(error.config.method,error.config.url,error.status,error.statusText));
                return null;
            });
    }
});

// Useful method to format string
String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};