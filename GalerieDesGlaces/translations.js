/**
 * Represent all the translations used in the application.
 * Feel free to change this as your convenience.
 *
 * NB : I know that better solution exist like angular-get-text,
 * but this solution that I choosed is quick and enough for this kind of small app.
 */

var translations = {
    OPEN_MENU: "Open the menu",
    DOWNLOAD_CURRENT_PICTURE: "Download current picture",
    CLOSE_MENU: "Close the menu",
    BRIDE_NAME: "Alice",
    GROOM_NAME: "Bob",
    CHOOSE_FILE_TO_SEE: "Please select a folder",
    WEDDING: "Wedding",
    ALL_FOLDER: "All",
    LOADING: "Loading",
    NEXT_PICTURE: 'Next picture',
    PREVIOUS_PICTURE : 'Previous picture',
    INFORMATION : 'Information',
    DID_YOU_KNOW: 'Did you know it?',
    INFORMATION_CONTENT: "You can use the arrow of your keyboard (left, right, up, bottom) to navigate through pictures. Moreover, you can hit the enter key in order to download the current picture.",
    GET_FOLDER_ERROR: "Error during getting picture's folders."
};
