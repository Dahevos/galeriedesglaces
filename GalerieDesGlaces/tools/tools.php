<?php
/**
 * Created by PhpStorm.
 * User: dahevos
 * Date: 08/07/15
 * Time: 08:37
 */

function getFolders($dir) {
    $folders = [];
    foreach(glob($dir . '/*', GLOB_ONLYDIR) as $dir) {
        $dirname = basename($dir);
        array_push($folders, $dirname);
    }
    return $folders;
}

function getPictures($dir) {
    $pictures = [];
    foreach(glob($dir . '/*.{jpeg,jpg,JPG,gif,png}', GLOB_BRACE) as $pic) {
        $picname = basename($pic);
        array_push($pictures, $picname);
    }
    return $pictures;
}

// we delete recursively all folders and file under dirPath, including itself
function deleteDir($dir) {

    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($files as $fileinfo) {
        $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
        $todo($fileinfo->getRealPath());
    }

    return rmdir($dir);
}

function imagecreatefromfile( $filename ) {
    if (!file_exists($filename)) {
        throw new InvalidArgumentException('File "'.$filename.'" not found.');
    }
    switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
        case 'jpeg':
        case 'jpg':
            return imagecreatefromjpeg($filename);
            break;

        case 'png':
            return imagecreatefrompng($filename);
            break;

        case 'gif':
            return imagecreatefromgif($filename);
            break;

        default:
            throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
            break;
    }
}

function imagesavefromfile( $filename, $picture, $dest_folder ) {
    if (!file_exists($filename)) {
        throw new InvalidArgumentException('File "'.$filename.'" not found.');
    }
    switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
        case 'jpeg':
        case 'jpg':
            return imagejpeg($picture, $dest_folder);
            break;

        case 'png':
            return imagepng($picture, $dest_folder);
            break;

        case 'gif':
            return imagegif($picture, $dest_folder);
            break;

        default:
            throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
            break;
    }
}