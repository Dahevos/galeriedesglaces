<?php
/**
 * Created by PhpStorm.
 * User: dahevos
 * Date: 08/07/15
 * Time: 08:30
 */


// We will get all pictures in the selected folder

include_once('../tools/constants.php');
include_once('../tools/tools.php');

// the user selected folder
if (isset($_GET['folder'])) {
    $selectedFolder = $_GET['folder'];
}
else {
    $selectedFolder = "GalerieDesGlaces_ALLFOLDER";
}

// all existing folder which is useful to be sure that the user selected folder exist and is present on the server
$folders = getFolders($DIR);

// the return object
$photos = array();

// We get all pictures if it's all, otherwise we get the picture of the selected folder
if ("GalerieDesGlaces_ALLFOLDER" == $selectedFolder) {
    foreach($folders as $folderName) {
        $folderPhotos = getPictures($DIR.'/'.$folderName);
        foreach($folderPhotos as $photo) {
            array_push($photos, $folderName.'/'.$photo);
        }
    }
    // scan also the picture folder
    $folderPhotos = getPictures($DIR);
    foreach($folderPhotos as $photo) {
        array_push($photos, '/'.$photo);
    }
}
else if (in_array($selectedFolder, $folders)) {
    $folderPhotos = getPictures($DIR.'/'.$selectedFolder);
    foreach($folderPhotos as $photo) {
        array_push($photos, $selectedFolder.'/'.$photo);
    }
}

print_r(json_encode($photos));
