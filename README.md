# README #

GalerieDesGlaces is a PHP / HTML / CSS / JS web app in order to display pictures.
Demo : http://galeriedesglaces.olympe.in/
This application was tested & fully compatible with Chrome (V43+), Firefox (39+), IE (11+).

### REQUIREMENT ###

- Apache server or similar
- PHP
- PHP GD lib
- Your pictures :-)

### INSTALLATION ###

- Upload the whole script on your server
- Upload your picture in the photos directory. You can organize them by creating folders. eg : photos/vin honneur, photos/church, photos/party. Be careful : this app support only one level folder (so don't create photos/church/part1/ folder ...)
- Feel free to edit translation.js in order to translate the application in your language
- go to your_website/GalerieDesGlaces/admin/ and launch the generating thumbmail process
- when it's done, edit the tools/constants.php file and set $ADMIN to false
- go to yourwebsite/GalerieDesGlaces : it's done !
- If you want to personalize the colour of the app, feel free to edit style.css

IMPORTANT : be sure that www-data (in case of APACHE) have right to access to the whole folder. sudo chown -R www-data GalerieDesGlaces/


### BUGS & FEEDBACK ###

Feel free to send your bug on the bug tracker system.

If you love this project you can pay me a tea : 
https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=BUC3NTRPRA54C